from django.contrib.auth import authenticate, login, logout
from django.http import Http404
from django.shortcuts import render, redirect
from django.shortcuts import get_object_or_404
from django.contrib.auth.decorators import login_required

from weblog.models import News
from weblog.models import Category
from weblog.models import Comment
from weblog.forms import NewsCreateForm
from weblog.forms import CommentCreateForm
from weblog.forms import CreateUserForm
from weblog.forms import UserLoginForm


# Create your views here.

@login_required(login_url='login')
def logout_view(request):
    logout(request)
    return redirect('login')


def login_view(request):
    next = request.GET.get('next')
    form = UserLoginForm()
    if request.method == 'POST':
        form = UserLoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)

            login(request, user)

            if next:
                return redirect(next)
            return redirect('news')

    return render(request, 'registration/login.html', {'form': form})


def NewsView(request):
    latest_news_list = News.objects.order_by('-pubdate')
    sport_news = News.objects.filter(topic='Sport')
    economy_news = News.objects.filter(topic='Economy')
    political_news = News.objects.filter(topic='Political')
    religion_news = News.objects.filter(topic='Religion')
    categories = Category.objects.all()
    context = {
        'categories': categories,
        'latest_news_list': latest_news_list,
        'sport_news': sport_news,
        'economy_news': economy_news,
        'political_news': political_news,
        'religion_news': religion_news,
    }
    return render(request, 'index.html', context)


@login_required(login_url='login')
def NewsDetailView(request, primary_key):
    try:
        primary_key = int(primary_key)
    except ValueError:
        raise Http404()
    news = get_object_or_404(News, pk=primary_key)
    comments = news.comments.all()
    context = {
        "news": news,
        "comments": comments,
    }
    return render(request, "news-detail.html", context)


@login_required(login_url='login')
def CreatNewPost(request):
    user = request.user
    if request.method == "POST":
        news_form = NewsCreateForm(data=request.POST)

        if news_form.is_valid():
            news = news_form.save(commit=False)
            news.user = user
            news.save()
            return redirect('news')
        else:
            print(news_form.errors)
    else:
        news_form = NewsCreateForm()

    # if request.method == "POST":
    #     body = request.POST()
    #     print(body)
    # elif request.method == "GET":
    #     print(request.method)

    # form = NewsCreateForm(request.POST)
    # if form.is_valid():
    #     form.save(commit=False)
    #     form = NewsCreateForm()
    context = {
        'form': news_form,
    }
    return render(request, 'news_edit.html', context)


@login_required(login_url='login')
def CategoryNews(request, category_id):
    try:
        category_id = int(category_id)
    except ValueError:
        raise Http404()

    category = get_object_or_404(Category, pk=category_id)
    news = News.objects.filter(category_id=category_id)
    context = {
        "category": category,
        "news": news,
    }
    return render(request, 'category.html', context)


@login_required(login_url='login')
def NewCommentView(request, news_id):
    user = request.user
    try:
        news_id = int(news_id)
    except ValueError:
        raise Http404()

    news = get_object_or_404(News, pk=news_id)

    if request.method == "POST":
        comment_form = CommentCreateForm(data=request.POST)
        if comment_form.is_valid():

            comment = comment_form.save(commit=False)
            comment.user = user
            comment.news = news
            comment.save()
            return redirect('news_detail', news_id)
        else:
            print(comment_form.errors)
    else:
        comment_form = CommentCreateForm()
    context = {
        "form": comment_form,
        "news": news,
    }
    return render(request, 'comment-create.html', context)


def register(request):
    form = CreateUserForm()
    if request.method == 'POST':
        form = CreateUserForm(request.POST)

        if form.is_valid():
            form.save()
            return redirect('news')
    else:
        form = CreateUserForm()

    context = {
        'form': form,
    }

    return render(request, 'register.html', context)
