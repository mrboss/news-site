"""work URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.urls import path, re_path
from django.conf.urls.static import static
from django.conf import settings
from django.contrib.auth.views import LoginView
from .views import NewsDetailView, NewsView, CreatNewPost, CategoryNews, NewCommentView, register, login_view, logout_view

urlpatterns = [
    path('logout/',  logout_view, name='logout'),
    path('login/', login_view, name='login'),
    path('register/', register, name='register'),
    path('news', NewsView, name='news'),
    re_path('news/(\d+)', NewsDetailView, name='news_detail'),
    path('new/post/', CreatNewPost, name='create_news'),
    re_path('news/categories/(\d+)/', CategoryNews, name='categories'),
    re_path('news/comment/(\d+)/', NewCommentView, name='create-comment'),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

