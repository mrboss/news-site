from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate

from weblog.models import News
from weblog.models import Comment


class UserLoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)

    def clean(self, *args, **kwargs):
        username = self.cleaned_data['username']
        password = self.cleaned_data['password']

        if username and password:
            user = authenticate(username=username, password=password)
            if not user:
                raise forms.ValidationError('this User does not exist')

            if not user.is_active:
                raise forms.ValidationError('This user is not active')
        return super(UserLoginForm, self).clean(*args, **kwargs)


class CreateUserForm(UserCreationForm):
    email = forms.EmailField(required=True)
    first_name = forms.CharField(max_length=200)
    last_name = forms.CharField(max_length=200)

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'username', 'email', 'password1', 'password2']

    def save(self, commit=True):
        user = super().save(commit=False)

        user.last_name = self.cleaned_data['last_name']
        user.first_name = self.cleaned_data['first_name']
        user.email = self.cleaned_data['email']

        if commit:
            user.save()

        return user


class NewsCreateForm(forms.ModelForm):
    title = forms.CharField(label="Title", initial="don't title", widget=forms.TextInput(
        attrs={'placeholder': 'Your title news'}
    )
                            )
    body = forms.CharField(required=False,
                           widget=forms.Textarea(
                               attrs={
                                   "placeholder": "Your Body news",
                                   "class": "new-class-name two",
                                   "id": "my-id-for-textarea",
                                   "rows": 10,
                                   "cols": 50,
                               }
                           )
                           )

    class Meta:
        model = News
        fields = ('title', 'body', 'summary', 'image', 'topic', 'category', 'reference')

    # def clean_title(self, *args, **kwargs):
    #     title = self.cleaned_data["title"]
    #     if "ABC" in title:
    #         return title
    #     else:
    #         raise forms.ValidationError('This is not a valid title')


class CommentCreateForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ('body',)

