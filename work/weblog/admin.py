from django.contrib import admin

from .models import News, Category, Comment

# Register your models here.
admin.site.register(News)


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'slug')
    prepopulated_fields = {'slug': ('name',)}


admin.site.register(Category)

admin.site.register(Comment)
