from django.db import models
from django.urls import reverse
from django.contrib.auth.models import User

# Create your models here.


class Category(models.Model):
    name = models.CharField(max_length=250)
    slug = models.SlugField(max_length=250, unique=True)

    class Meta:
        ordering = ('name',)
        verbose_name = 'category'
        verbose_name_plural = 'categories'

    def __str__(self):
        return self.name


class News(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='NewsUser')
    category = models.ForeignKey(Category, on_delete=models.CASCADE, default='sport')
    body = models.TextField()
    summary = models.CharField(max_length=400)
    title = models.CharField(max_length=250)
    create = models.DateTimeField(auto_now_add=True)
    reference = models.CharField(max_length=250)
    image = models.ImageField(upload_to='media', blank=True, null=True)

    def get_comments(self):
        return self.comments

    @property
    def image_url(self):
        if self.image and hasattr(self.image, 'url'):
            return self.image.url

    TOPIC_CHOICE = [
        ('Sport', 'sport'),
        ('Economy', 'economy'),
        ('Political', 'political'),
        ('Religion', 'religion'),
    ]
    topic = models.CharField(max_length=10, choices=TOPIC_CHOICE, default='Sport')

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        reverse('news_detail', args=[str(self.id)])


class Comment(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='CommentUser')
    news = models.ForeignKey(News, on_delete=models.CASCADE, related_name='comments')
    body = models.TextField(max_length=400)
    create = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-create']

    def __str__(self):
        return self.body
