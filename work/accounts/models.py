from django.db import models
from django.contrib.auth.models import AbstractBaseUser


# Create your models here.

# class User(AbstractBaseUser):
#     email = models.EmailField(unique=True)
#     nick_name = models.CharField(max_length=130)
#     last_name = models.CharField(max_length=130)
#     active = models.BooleanField(default=True)
#     staff = models.BooleanField(default=False)
#     admin = models.BooleanField(default=False)
#     timestamp = models.DateTimeField(auto_now_add=True)
#
#     USERNAME_FIELD = 'email'  # username
#     # USERNAME_FIELD and password are required by default
#     REQUIRED_FIELDS = []
#
#     def __str__(self):
#         return "email: {}, full name: {}{}, ".format(self.email, self.last_name, self.nick_name)
#
#     def get_full_name(self):
#         return "full name: {}{}, ".format(self.last_name, self.nick_name)
#
#     def get_short_name(self):
#         return self.nick_name
#
#     @property
#     def is_staff(self):
#         return self.staff
#
#     @property
#     def is_active(self):
#         return self.active
#
#     @property
#     def is_admin(self):
#         return self.admin
#

# class Profile(models.Model):
#     user = models.OneToOneField(User, on_delete=models.CASCADE)
#     # extend extra data
